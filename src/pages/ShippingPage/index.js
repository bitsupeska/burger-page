import React, { useContext, useState } from "react"
import { Route } from "react-router";
import Burger from "../../components/Burger"
import ContactData from "../../components/ContactData";
import Button from "../../components/General/Button";
import css from "./style.module.css"
import BurgerContext from "../../context/BurgerContext"
const ShippingPage = (props) => {
    const ctx = useContext(BurgerContext)
    // componentWillMount() {
    //     const query = new URLSearchParams(props.location.search);
    //     const ingredients = {};
    //     let price = 0;
    //     for (let param of query.entries()) {
    //         console.log(param)
    //         if (param[0] === "dun") {
    //             price = param[1];
    //         } else {
    //             ingredients[param[0]] = param[1];
    //         }
    //     }
    //     setState({ ingredients, price })
    //     console.log(ingredients
    // }
    const goBack = () => {
        props.history.goBack();
    }
    const showContactData = () => {
        props.history.replace("/ship/contact");
    }
    return <div className={css.ShippingPage}>
        <p style={{ fontSize: "24px" }}><strong>Таны захиалга амжилттай болно гэж итгэж байна</strong></p>
        <p style={{ fontSize: "24px" }}><strong>{ctx.burger.totalPrice}</strong></p>
        <Burger></Burger>
        <Button daragdsan={goBack} btnType={"Danger"} text={"ЗАХИАЛГЫГ ЦУЦЛАХ"}></Button>
        <Button daragdsan={showContactData} btnType={"Success"} text={"ХҮРГЭЛТИЙН МЭДЭЭЛЭЛ ОРУУЛАХ"}></Button>

        {/* <Route path="/ship/contact" >
                <ContactData ingredients={state.ingredients} price={state.price} />
            </Route> */}

        <Route path="/ship/contact" render={() => (<ContactData />)} />
    </div>

}

export default ShippingPage