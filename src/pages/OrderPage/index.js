import React, { useContext, useEffect } from "react"
import Spinner from "../../components/General/Spinner"
import Order from "../../components/Order"
import UserContext from "../../context/UserContext"
import OrderContext from "../../context/OrderContext"
const OrderPage = (props) => {
    const orderContext = useContext(OrderContext);
    const userContext = useContext(UserContext);
    useEffect(() => {
        orderContext.loadOrders(userContext.state.userId, userContext.state.token)
    }, [])
    return <div>
        {orderContext.state.loading ? <Spinner></Spinner> : orderContext.state.orders.map(el => <Order key={el[0]}
            order={el[1]} />
        )}
    </div>
}

export default OrderPage;