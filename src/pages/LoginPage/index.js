import React, { useContext, useState } from "react"
import Button from "../../components/General/Button"
import css from "./style.module.css"
import Spinner from "../../components/General/Spinner"
import { Redirect } from "react-router-dom"
import UserContext from "../../context/UserContext"
const Login = (props) => {
    const ctx = useContext(UserContext)
    const [form, setForm] = React.useState({
        email: "",
        password: ""
    })
    const login = () => {
        ctx.loginUser(form.email, form.password)
    }
    const changeEmail = (e) => {
        const newEmail = e.target.value
        setForm(prevState => ({
            email: newEmail,
            password: prevState.password
        }))
    }
    const changePassword = (e) => {
        const newPassword = e.target.value
        setForm(prevState => ({
            email: prevState.email,
            password: newPassword
        }))
    }
    return <div className={css.Login}>
        {ctx.state.userId && <Redirect to="/orders"></Redirect>}
        <input onChange={changeEmail} type="text" placeholder="Имэйл хаяг"></input>
        <input onChange={changePassword} type="password" placeholder="Нууц үг"></input>
        {ctx.state.loginIn && <Spinner></Spinner>}
        {ctx.state.error && <div style={{ color: "red" }}>{ctx.state.error}</div>}
        <Button text="Логин" btnType="Success" daragdsan={login}></Button>
    </div>
}
export default Login;
