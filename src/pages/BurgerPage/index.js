import React, { useState } from "react";

import Burger from "../../components/Burger";
import BuildControls from "../../components/BuildControls";
import Modal from "../../components/General/Modal";
import OrderSummary from "../../components/OrderSummary";


const BurgerBuilder = (props) => {
  const [confirmOrder, setConfirmOrder] = useState(false);

  const continueOrder = () => {
    const params = [];
    for (let orts in props.burgerOrts) {
      params.push(orts + "=" + props.burgerOrts[orts]);
    }
    params.push("dun" + "=" + props.niitune)
    props.history.push("/ship");
    closeConfirmModal();
  };

  const showConfirmModal = () => {
    setConfirmOrder(true);
  };

  const closeConfirmModal = () => {
    setConfirmOrder(false);
  };

  return (
    <div>
      <Modal
        closeConfirmModal={closeConfirmModal}
        show={confirmOrder}
      >
        <OrderSummary
          onCancel={closeConfirmModal}
          onContinue={continueOrder}
        />
      </Modal>
      <Burger />
      <BuildControls
        showConfirmModal={showConfirmModal}
      />
    </div>
  );
}

export default BurgerBuilder;
