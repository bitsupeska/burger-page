import React, { useContext, useEffect, useState } from "react"
import Button from "../../components/General/Button"
import css from "./style.module.css"
import Spinner from "../../components/General/Spinner"
import { Redirect } from "react-router-dom"
import UserContext from "../../context/UserContext"
const Signup = (props) => {
    const ctx = useContext(UserContext)
    const [email, setEmail] = useState("")
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [error, setError] = useState("")

    useEffect(() => {
        // if (email == "") {
        //     setError("email oruulna uu")
        // } else {
        //     setError("")
        // }
    }, [email])
    const signup = () => {
        if (password1 === password2) {
            ctx.signupUser(email, password1)
        } else {
            setError("Нууц үг таарахгүй байна")
        }
    }
    return <div className={css.Signup}>
        {ctx.state.userId && <Redirect to="/orders"></Redirect>}
        <h1>Бүртгэлийн форм</h1>
        <div>Тө өөрийнхөө мэдээллээ оруулна уу</div>
        <input onChange={e => setEmail(e.target.value)} type="text" placeholder="Имэйл хаяг"></input>
        <input onChange={e => setPassword1(e.target.value)} type="password" placeholder="Нууц үгээ оруулна уу"></input>
        <input onChange={e => setPassword2(e.target.value)} type="password" placeholder="Нууц үгээ давтан оруулна уу"></input>
        {error && <div style={{ color: "red" }}>{error}</div>}
        {ctx.state.firebaseError && <div style={{ color: "red" }}>{ctx.state.firebaseError}</div>}
        {ctx.state.saving && <Spinner></Spinner>}
        <Button text="Бүртгүүлэх" btnType="Success" daragdsan={signup}></Button>
    </div>
}

export default Signup;
