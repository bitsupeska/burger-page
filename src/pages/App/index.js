import React, { useState, useEffect, Suspense, useContext } from "react";
import css from "./style.module.css";

import Toolbar from "../../components/Toolbar";
import SideBar from "../../components/SideBar";
import { Redirect, Route, Switch } from "react-router-dom";
import ShippingPage from "../ShippingPage";
import Login from "../LoginPage";
import Logout from "../../components/Logout";
import { BurgerStore } from "../../context/BurgerContext"
import { OrderStore } from "../../context/OrderContext"
import UserContext from "../../context/UserContext"
const BurgerPage = React.lazy(() => {
  return import("../BurgerPage")
})
const OrderPage = React.lazy(() => {
  return import("../OrderPage")
})
const Signup = React.lazy(() => {
  return import("../SignupPage")
})

const App = (props) => {
  const userCtx = useContext(UserContext);
  const [showSidebar, setShowSidebar] = useState(false);


  const toggleSideBar = () => {
    setShowSidebar(prevState => !prevState)
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    const userId = localStorage.getItem("userId");
    const expireDate = new Date(localStorage.getItem("expireDate"));
    const refreshToken = localStorage.getItem("refreshToken");
    if (token) {
      if (expireDate > new Date()) {
        //Token hugatsaa duusaagui
        userCtx.loginUserSuccess(token, userId, expireDate, refreshToken);
        //Token huchintei bgaa hugatsaa
        const difMs = expireDate.getTime() - new Date().getTime();
        //Token huchintei bgaa hugatsaand auto logout
        userCtx.autoRenewAfterMillisec(difMs);
      } else {
        //Token hugatsaa ni duussan
        // userCtx.logout();//shuud haana
        userCtx.autoRenewAfterMillisec(3600 * 1000);
      }
    }
  }, [])
  return (
    <div>
      <Toolbar toggleSideBar={toggleSideBar} />

      <SideBar
        showSidebar={showSidebar}
        toggleSideBar={toggleSideBar}
      />

      <main className={css.Content}>

        <BurgerStore>
          <Suspense fallback={<div>Түр хүлээнэ үү</div>}>
            {userCtx.state.userId ? (<Switch>

              <Route path="/logout" component={Logout}></Route>
              <Route path="/orders" >
                <OrderStore>
                  <OrderPage></OrderPage>
                </OrderStore>
              </Route>
              <Route path="/ship" component={ShippingPage}></Route>
              <Route path="/" component={BurgerPage}></Route>
            </Switch>) : (<Switch>
              <Route path="/login" component={Login}></Route>
              <Route path="/signup" component={Signup}></Route>
              <Redirect to="/login"></Redirect>
            </Switch>)}
          </Suspense>
        </BurgerStore>

      </main>
    </div >
  );
}


// const mapDispatchToProps = dispatch => {
//   return {
//     autoLogin: (token, userId) => dispatch(actions.loginUserSuccess(token, userId)),
//     logout: () => dispatch(singupActions.logout()),
//     autoLogoutAfterMillisec: (difMs) => dispatch(singupActions.autoLogoutAfterMillisec(difMs))
//   }
// }
export default App;
