import React, { useState } from "react"
import axios from '../axios-orders';
const initialState = {
    saving: false,
    loginIn: false,
    error: null,
    errorCode: null,
    token: null,
    userId: null,
    expireDate: null,
}
const UserContext = React.createContext();

export const UserStore = props => {
    const [state, setState] = useState(initialState);
    const loginUserSuccess = (token, userId, expireDate, refreshToken) => {
        localStorage.setItem("token", token);
        localStorage.setItem("userId", userId);
        localStorage.setItem("expireDate", expireDate);
        localStorage.setItem("refreshToken", refreshToken);
        setState({ ...state, loginIn: false, error: null, errorCode: null, token, userId, expireDate })
    }
    const logout = () => {
        localStorage.removeItem("token");
        localStorage.removeItem("userId");
        localStorage.removeItem("expireDate");
        localStorage.removeItem("refreshToken");
        setState(initialState);
    }
    const signupUser = (email, password) => {
        setState({ ...state, saving: true });
        const data = {
            email, password, returnSecureToken: true
        }
        axios.post("https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAfvPsNh1yW6vp4zXtCmeEamyamInHWU3c", data).then(result => {
            const token = result.data.idToken;
            const userId = result.data.localId;
            // const expiresIn = result.data.expiresIn;
            // const expireDate = new Date(new Date().getTime() + expiresIn * 1000)
            // const refreshToken = result.data.refreshToken;
            localStorage.setItem("token", token);
            localStorage.setItem("userId", userId);
            // localStorage.setItem("expireDate", expireDate);
            // localStorage.setItem("refreshToken", refreshToken);
            setState({ ...state, saving: false, token, userId, error: null, errorCode: null })

        }).catch(err => {
            setState({ ...state, saving: false, error: err.response.data.error.message, errorCode: err.response.data.error.code, token: null, userId: null })
        })

    }



    //token shinchleh
    const autoRenewAfterMillisec = (ms) => {
        axios.post("https://securetoken.googleapis.com/v1/token?key=AIzaSyAfvPsNh1yW6vp4zXtCmeEamyamInHWU3c", {
            grant_type: "refresh_token",
            refresh_token: localStorage.getItem("refreshToken")
        }).then(result => {
            const token = result.data.id_token;
            const userId = result.data.user_id;
            const expiresIn = result.data.expires_in;
            const expireDate = new Date(new Date().getTime() + expiresIn * 1000)
            const refreshToken = result.data.refresh_token;
            loginUserSuccess(token, userId, expireDate, refreshToken);
        }).catch(err => {

            setState({ ...state, loginIn: false, error: err.message, errorCode: err.code, token: null, userId: null })
        })
        setTimeout(() => {
            autoRenewAfterMillisec(3600 * 1000)//shinchlene
            //logout() shuud haana
        }, ms)
    }

    const loginUser = (email, password) => {
        setState({ ...state, loginIn: true })
        const data = {
            email, password, returnSecureToken: true
        }
        axios.post("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAfvPsNh1yW6vp4zXtCmeEamyamInHWU3c", data).then(result => {
            // Local storage hadgalah
            const token = result.data.idToken;
            const userId = result.data.localId;
            const expiresIn = result.data.expiresIn;
            const expireDate = new Date(new Date().getTime() + expiresIn * 1000)
            const refreshToken = result.data.refreshToken;
            loginUserSuccess(token, userId, expireDate, refreshToken);
        }).catch(err => {
            setState({ ...state, loginIn: false, error: err.message, errorCode: err.code, token: null, userId: null })
        })
    }
    return (
        <UserContext.Provider value={{ state, signupUser, loginUser, logout, loginUserSuccess, autoRenewAfterMillisec }}>
            {props.children}
        </UserContext.Provider>
    )
}
export default UserContext;