import React, { useState } from "react"
import axios from "../axios-orders"


const OrderContext = React.createContext();

const initialState = {
    orders: [],
    loading: false,
    error: null,
}
export const OrderStore = props => {
    const [state, setState] = useState(initialState);
    const loadOrders = (userId, token) => {
        //Захиалгыг татаж эхэллээ
        //Энийг хүлээж аваад Spinner ажилиж эхлэнэ
        setState({ ...state, loading: true })
        axios.get(`orders.json?&auth=${token}&orderBy="userId"&equalTo="${userId}"`).then(res => {
            setState({
                ...state,
                loading: false,
                orders: Object.entries(res.data).reverse()
            })
        }).catch(err => {
            setState({
                ...state,
                loading: false,
                error: err
            })
        })
    }
    return (
        <OrderContext.Provider value={{ state, loadOrders }}>
            {props.children}
        </OrderContext.Provider>
    )
}
export default OrderContext;