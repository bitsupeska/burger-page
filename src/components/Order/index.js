import React from "react"
import css from "./style.module.css"
const Order = (props) => {
    console.log("order", props.order)
    return <div className={css.order}>
        <p>Орц: {props.order.orts.bacon} | Салад: {props.order.orts.salad}  |  Бяслаг :{props.order.orts.cheese}</p>
        <p>Хаяг: {props.order.hayag.name} | {props.order.hayag.street}  |  {props.order.hayag.city}</p>
        <p>Үнийн дүн : <strong>{props.order.dun}</strong></p>

    </div>
}
export default Order;
