import React, { useContext, useEffect, useRef, useState } from "react"
import Button from "../General/Button"
import css from "./style.module.css"
import Spinner from "../General/Spinner"
import { useHistory } from "react-router-dom"
import BurgerContext from "../../context/BurgerContext"
import UserContext from "../../context/UserContext"
const ContactData = props => {
    const history = useHistory();
    const ctx = useContext(BurgerContext);
    const userCtx = useContext(UserContext);
    const [name, setName] = useState();
    const [city, setCity] = useState();
    const [street, setStreet] = useState();

    const dunRef = useRef()
    useEffect(() => {
        if (ctx.burger.finished && !ctx.burger.error)
            history.replace("/orders")
        return () => {
            //Цэвэрлэгч функц : Захиалгыг хоослоно
            if (ctx.burger.finished)
                ctx.saveBurger();
        }
    }, [ctx.burger.finished])
    const changeName = (e) => {
        if (dunRef.current.style.color === 'red') {
            dunRef.current.style.color = 'green'
        } else {
            dunRef.current.style.color = 'red'
        }
        setName(e.target.value)
    }
    const changeStreet = (e) => {
        setStreet(e.target.value)
    }
    const changeCity = (e) => {
        setCity(e.target.value);
    }

    const saveOrder = () => {
        const order = {
            userId: userCtx.state.userId,
            orts: ctx.burger.ingredients,
            dun: ctx.burger.totalPrice,
            hayag: {
                name,
                city,
                street
            }
        }
        ctx.saveBurger(order, userCtx.state.token);

    }
    console.log("contact render")
    return <div className={css.ContactData}>
        <div ref={dunRef}>
            <strong style={{ fontSize: "16px" }}>Дүн : {ctx.burger.totalPrice}</strong>
        </div>
        {ctx.burger.error && `Захиалах явцад алдаа гарлаа : ${ctx.burger.error}`}
        {ctx.burger.saving ? <Spinner></Spinner> : (<div>
            <input onChange={changeName} type="text" name="name" placeholder="таний нэр"></input>
            <input onChange={changeStreet} type="text" name="street" placeholder="таний гэрийн хаяг"></input>
            <input onChange={changeCity} type="text" name="city" placeholder="таны хот"></input>
            <Button daragdsan={saveOrder} text="ИЛГЭЭХ" btnType="Success"></Button>
        </div>)}
        <Button daragdsan={ctx.toggle} text="TOGGLE" btnType="Success"></Button>
    </div>
}
export default ContactData;